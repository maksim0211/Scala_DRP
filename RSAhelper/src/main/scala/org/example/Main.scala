package org.example
import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import java.security.{KeyPair, _}
import java.util

import javax.crypto._
object Main {
  var data : String = ""
  protected val cipher = Cipher.getInstance("RSA")


  def main(args: Array[String]): Unit = {
    //Принимаю пароль
    data = args(0)

    //Сериализую зашифрованный пароль
    try {
      val oos = new ObjectOutputStream(new FileOutputStream("src\\main\\resources\\logpass.dat"))

      try {
        oos.writeObject(encrypt())
        System.out.println("File has been written")
      } catch {
        case ex: Exception =>
          System.out.println(ex.getMessage)
      } finally if (oos != null) oos.close()
    }
  }

  //Шифрование
  def encrypt() = {
    cipher.init(Cipher.ENCRYPT_MODE,getKeyPair()._1)
    cipher.doFinal(data.getBytes())
  }

  //Считываю заранее сериализованные ключи для RSA
  def getKeyPair(): (Key, Key) = {
    try {
      val ois = new ObjectInputStream(new FileInputStream("src\\main\\resources\\rsa.dat"))
      ois.readObject().asInstanceOf[(Key, Key)]
    }
  }
}

/*def getKeyPairOld(): (Key, Key) = {
  val kp = KeyPairGenerator.getInstance("RSA").generateKeyPair()
  (kp.getPublic, kp.getPrivate)

}

  def decrypt(str : Array[Byte]) = {
    cipher.init(Cipher.DECRYPT_MODE,getKeyPair()._2)
    val decrypted = cipher.doFinal(str)
    decrypted.map(_.toChar).mkString
  }
*/
