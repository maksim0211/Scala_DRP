<h3>Задача по репликации данных OracleDB => Hadoop </h3>

### V1.0:

message spark_schema {
-  optional fixed_len_byte_array(16) ID (DECIMAL(38,10));
-  optional binary NAME (UTF8);
-  optional binary SURNAME (UTF8);
-  optional binary NUM_ACCOUNT (UTF8);
-  optional binary CURRENCY (UTF8);
-  optional binary EMAIL (UTF8);
-  optional binary COUNTRY (UTF8);
-  optional int96 CHANGE_DTTM;
}

### Пакеты: 

- load
    - Loader - загрузка данных из Oracle
- system
    - Parameters - параметры SparkSession, Properties и парсинг args из функции main(...)
    - RSADecoder - десериализует зашифрованные ключи RSA и пароль/логин из HDFS
- table
    - SavFctUserBill - StructType и case class моей таблицы
- unload
    - Unloader - выгружаем данные в Hive из Oracle

Main - класс инициализирующий работу Spark.

### V1.1

- Вместо статической обработки таблицы теперь это происходит динамически.
- Вместо одной таблицы теперь возможно обрабатывать несколько.
- Удален класс SavFctUserBill - вместо него SchemaIdentifier
- Необходимые таблицы для обработки берутся из "таблицы имен этих таблиц" в Oracle
- Теперь CHANGE_DTTM фильтруется на стороне Oracle
- Не обязательно наличие существующей таблицы в Hive

#### Приоритет, что надо сделать: параллельная загрузка

### V1.2

- Осуществляется параллельная загрузка
- Количество нод = параллельных потоков для загрузки задается в конфиге
   
### V1.3

- Вместо "таблицы имен таблиц" в Oracle используется файл в HDFS
- Таблицы условно поделены на два типа:
    - справочник - поля change_dttm нет, ведется загрузка по полному срезу
    - журнал - если грузится в первый то по полному срезу, иначе инкрементально
