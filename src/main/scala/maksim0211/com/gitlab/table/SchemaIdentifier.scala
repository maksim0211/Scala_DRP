package maksim0211.com.gitlab.table

import maksim0211.com.gitlab.system.Parameters
import maksim0211.com.gitlab.load.Loader.{loadSysTableFromOracle}


object SchemaIdentifier {
  def makeCols(param : Parameters): String = {
    val all_fields: Array[String] = getTableInfo(param)
      .map(field =>
        field._2 match {
          case "DATE" => s"""${field._1}"""
          case "TIMESTAMP" => s"""${field._1}"""
          case "VARCHAR2" => s"""${field._1}"""
          case "NUMBER" => s"""to_char(${field._1}) as ${field._1}"""
          case "FLOAT" => s"""to_char(${field._1} ) as ${field._1}"""
          case _ => s"""to_char(${field._1} ) as ${field._1}"""
        })
    all_fields.mkString(",")
  }

  def getTableInfo(param : Parameters): Array[(String, String, Int, Int, Int, Int)] = {
    loadSysTableFromOracle(param)
      .collect()
      .map(row =>
      (
        row.getAs[String]("COLUMN_NAME"),
        row.getAs[String]("DATA_TYPE"),
        row.getAs[Int]("DATA_LENGTH"),
        row.getAs[Int]("DATA_PRECISION"),
        row.getAs[Int]("DATA_SCALE"),
        row.getAs[Int]("COLUMN_ID")
      )
    )
  }
}
