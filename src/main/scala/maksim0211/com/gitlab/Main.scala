package maksim0211.com.gitlab

import maksim0211.com.gitlab.load.Loader.{readTableListFromHdfs}
import maksim0211.com.gitlab.system.Parameters
import maksim0211.com.gitlab.unload.Unloader._
object Main {

  def main(args: Array[String]): Unit = {
    implicit val param: Parameters = new Parameters(args)
    //Считываем "таблицу с таблицами" и получаем необходимые базы данных
    val tableList = readTableListFromHdfs(param)
    //Каждую таблицу выгружаем в hadoop
    for(record <- tableList) {
      val table = record.substring(0,record.indexOf(","))
      param.setOracleTableName(table)
    /*
    если второе поле 0 - это таблица справочник, грузить всегда по полному срезу
    если второе поле 1 - это таблица журнал,
    в 1 раз грузить по полному срезу(ну если таблицы такой в хайв не существует), в последующие инкрементально
    */
      if(record.endsWith("1") &&
        param.sc.sqlContext.tableNames(param.HIVE_DB_NAME).contains(param.getOracleTableName()+"_h"))
        unloadPart(param)
      else unloadFull(param)
    }
  }
}
