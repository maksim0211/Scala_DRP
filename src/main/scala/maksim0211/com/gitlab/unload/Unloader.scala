package maksim0211.com.gitlab.unload

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import maksim0211.com.gitlab.load.Loader.{loadFromOracleCastedCols, loadFromOracleSqlContext}
import maksim0211.com.gitlab.system.Parameters
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions.{col, from_unixtime, lit, to_timestamp}

object Unloader {

  //загружаем все данные
  def unloadFull(param : Parameters) ={
    val df = loadFromOracleCastedCols(param)
      .drop(param.PARTITION_COLUMN)
    df
      .write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .saveAsTable(param.HIVE_DB_NAME+"."+param.getOracleTableName+"_H")
  }

  //загружаем данные по условию
  def unloadPart(param : Parameters): Unit ={
      //Берем из системных данных таблицы в hive последнюю ddl операцию(загрузку) - timestamp
      val lastDdlTimeDate = param.sc.sqlContext.sql("SHOW TBLPROPERTIES " +
        param.HIVE_DB_NAME + "." + param.getOracleTableName + "_H ('transient_lastDdlTime')")
        .withColumn("transient_lastDdlTime", to_timestamp(from_unixtime(col("value"))))
        .select("transient_lastDdlTime")
        .head()
        .getTimestamp(0)
        .toString
        .replace('.', ',')
        .replace("-", ".")

    //Оставляем строки, подходящие по условию
    val sql0 =
      s"""
         |  (SELECT change_dttm, mod(rownum,${param.NUM_PARTITIONS}) as ${param.PARTITION_COLUMN} FROM ${param.getOracleTableName()}
         |  WHERE change_dttm >= to_timestamp('${lastDdlTimeDate.substring(0,lastDdlTimeDate.length-2)}','yyyy.MM.dd HH24:MI:SS')
         |  AND change_dttm < to_timestamp('${(lit(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm:ss").format(LocalDateTime.now)))}','dd.MM.yy HH24:MI:SS'))
         |""".stripMargin

    val newDataDF = loadFromOracleSqlContext(param,sql0)
      .drop(param.PARTITION_COLUMN)
    //записываем их, если есть что)
    if(newDataDF.count()>0) {
      newDataDF
        .write
        .format("hive")
        .mode(SaveMode.Append)
        .insertInto(param.HIVE_DB_NAME + "." + param.getOracleTableName + "_H")
    }
    else {
      println("empty data")
    }
  }
}
