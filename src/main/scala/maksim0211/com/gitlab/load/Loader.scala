package maksim0211.com.gitlab.load

import maksim0211.com.gitlab.system.Parameters
import maksim0211.com.gitlab.table.SchemaIdentifier.makeCols
import org.apache.spark.sql.DataFrame

object Loader {

  // Читаем с hdfs имя таблицы и тип загрузки
  def readTableListFromHdfs(param : Parameters) ={
    println(param.HDFS_TABLEPROP_NAME)
    param.sc.sparkContext
      .textFile(param.HDFS_TABLEPROP_NAME)
      .collect()
      .toList

  }

  //Загрузка системной таблицы из ораклдб
  def loadSysTableFromOracle(param: Parameters) : DataFrame = {
    val sqlQ =
      s""" (SELECT COLUMN_NAME, DATA_TYPE,
         |    CAST(nvl(DATA_LENGTH, 0) as INTEGER) as DATA_LENGTH,
         |    CAST(nvl(DATA_PRECISION, 38) as INTEGER) as DATA_PRECISION,
         |    CAST(nvl(DATA_SCALE, 10) as INTEGER) as DATA_SCALE,
         |    CAST(nvl(COLUMN_ID, 0) as INTEGER) as COLUMN_ID
         |  FROM ALL_TAB_COLUMNS
         |  WHERE OWNER = '${param.ORACLE_OWNER_NAME}'
         |    AND TABLE_NAME = '${param.getOracleTableName.toUpperCase}'
         |  ORDER BY COLUMN_ID)""".stripMargin

    param.sc.read
      .jdbc(param.URL, sqlQ,param.prop)
  }

  //Загрузка таблицы из ораклдб с помощью вложенного запроса
  def loadFromOracleSqlContext(param : Parameters, sqlText : String)  = {
    param.sc.read
      .options(param.mapOfOptions)
      .jdbc(param.URL, sqlText,param.prop)
  }

  //загрузка таблицы из ораклдб с приведенными к нужным типам столбцами
  def loadFromOracleCastedCols(param : Parameters)  = {

    val sqlQ2 =
      s""" (SELECT ${makeCols(param)}, mod(rownum,${param.NUM_PARTITIONS}) as ${param.PARTITION_COLUMN}
         | FROM ${param.getOracleTableName} )""".stripMargin
    param.sc.read
      .options(param.mapOfOptions)
      .jdbc(param.URL, sqlQ2,param.prop)
  }
}
