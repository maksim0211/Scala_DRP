package maksim0211.com.gitlab.system

import java.util.Properties
import maksim0211.com.gitlab.system.RSADecoder.decrypt
import org.apache.spark.sql.SparkSession

class Parameters(args : Array[String]) {

    val argsMap: Map[String, String] = initArgsMap()

    private def pair(arg: String): (String, String) = {
        val kv = arg.split("=", -1)
        (kv(0), kv(1))
    }

    def initArgsMap(): Map[String,String] ={
       args.map(pair).toMap
    }

    def setOracleTableName(name : String): Unit ={
        ORACLE_TABLE_NAME = name
    }

    def getOracleTableName() = ORACLE_TABLE_NAME

    val sc = SparkSession.builder()
      .appName("Scala_DRP")
      .master("yarn")
      .enableHiveSupport()
      .getOrCreate()

    val URL: String = argsMap.getOrElse("URL", "")
    val HDFS_TABLEPROP_NAME: String = argsMap.getOrElse("HDFS_TABLEPROP_NAME", "")
    val ORACLE_OWNER_NAME: String = argsMap.getOrElse("ORACLE_OWNER_NAME", "")
    val HIVE_DB_NAME: String = argsMap.getOrElse("HIVE_DB_NAME", "")
    val PATH_KEYPAIR: String = argsMap.getOrElse("PATH_KEYPAIR", "")
    val PATH_LOGPASS: String = argsMap.getOrElse("PATH_LOGPASS", "")
    val PARTITION_COLUMN: String = argsMap.getOrElse("PARTITION_COLUMN", "")
    val NUM_PARTITIONS: String = sc.conf.get("spark.executor.instances")
    val NUM_FETCHSIZE: String = argsMap.getOrElse("NUM_FETCHSIZE", "")
    println(NUM_PARTITIONS)

    private var ORACLE_TABLE_NAME : String = "";
    val mapOfOptions = Map(
        "partitionColumn" -> PARTITION_COLUMN,
        "lowerBound" -> "0",
        "upperBound" -> {NUM_PARTITIONS.toInt-1}.toString,
        "numPartitions" -> NUM_PARTITIONS,
        "fetchsize" -> NUM_FETCHSIZE
    )

    val prop = new Properties()
    prop.setProperty("user", decrypt(PATH_KEYPAIR,PATH_LOGPASS).mkString)
    prop.setProperty("password",decrypt(PATH_KEYPAIR,PATH_LOGPASS).mkString)
    prop.setProperty("driver", "oracle.jdbc.driver.OracleDriver")

}

