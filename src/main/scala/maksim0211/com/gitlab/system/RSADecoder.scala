package maksim0211.com.gitlab.system

import java.io.ObjectInputStream
import java.security._
import javax.crypto._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

object RSADecoder {

  protected val cipher = Cipher.getInstance("RSA")

  def decrypt(PATH_KEYPAIR : String,PATH_LOGPASS : String) = {
    cipher.init(Cipher.DECRYPT_MODE,getKeyPair(PATH_KEYPAIR)._2)
    val decrypted = cipher.doFinal(getLogPass(PATH_LOGPASS))
    decrypted.map(_.toChar).mkString
  }

  def getKeyPairOld(): (Key,Key) ={
    val kp = KeyPairGenerator.getInstance("RSA").generateKeyPair()
    (kp.getPublic,kp.getPrivate)

  }
  def getKeyPair(PATH_KEYPAIR : String): (Key,Key) ={
    try
    {
      val fs = FileSystem.get(new Configuration());
      val in = fs.open(new Path(PATH_KEYPAIR));
      val ois = new ObjectInputStream(in)
      val res = ois.readObject().asInstanceOf[(Key,Key)]
      ois.close()
      in.close()
      res
    }
  }
  def getLogPass(PATH_LOGPASS : String): Array[Byte] ={
    try
      {
        val fs = FileSystem.get(new Configuration());
        val in = fs.open(new Path(PATH_LOGPASS));
        val ois = new ObjectInputStream(in)
        val res = ois.readObject().asInstanceOf[Array[Byte]]
        ois.close()
        in.close()
        res
      }
  }
}