#!/bin/bash
spark-submit \
--class "maksim0211.com.gitlab.Main" \
--jars /user/sav/drp/hive-jdbc-1.1.0.jar \
--jars /user/sav/drp/ojdbc8-19.7.0.0.jar \
--num-executors 8 \
user/sav/drp/Scala_DRP-1.0.jar \
URL=jdbc:oracle:thin:@192.168.233.20:1521:DWH \
HDFS_TABLEPROP_NAME=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/tableprop.txt \
ORACLE_OWNER_NAME=DWH \
HIVE_DB_NAME=test_msavelev \
PATH_KEYPAIR=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/resources/rsa.dat \
PATH_LOGPASS=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/resources/logpass.dat \
PARTITION_COLUMN=p_column \
NUM_FETCHSIZE=100000