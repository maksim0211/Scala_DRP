import os
import sys
import datetime as dt
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.operators.email_operator import EmailOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime
from tempfile import NamedTemporaryFile

os.environ['SPARK_HOME'] = '/opt/cloudera/parcels/SPARK2-2.3.0.cloudera4-1.cdh5.13.3.p0.611179/lib/spark2'
os.environ['JAVA_HOME'] = '/usr/java/jdk1.8.0_151'
os.environ['HADOOP_USER_NAME'] = 'root'
sys.path.append(os.path.join(os.environ['JAVA_HOME'], 'bin'))
sys.path.append(os.path.join(os.environ['SPARK_HOME'], 'bin'))


def build_email_suc(self):
        email_op = EmailOperator(
            task_id='send_email_suc',
            to="maksim0211@gmail.com",
            subject="Test Email Please Ignore",
            html_content=""" <h3>Success</h3> """
        )
        email_op.execute(self)

def build_email_fail(self):
        email_op = EmailOperator(
            task_id='send_email_fail',
            to="maksim0211@gmail.com",
            subject="Test Email Please Ignore",
            html_content=""" <h3>Error</h3> """
        )
        email_op.execute(self)

default_args = {
    "owner": "airflow",
    "start_date": dt.datetime(2020, 4, 18),
    "retries": 3,
    "retry_delay": dt.timedelta(minutes=1),
    "depends_on_past": False,
    'on_success_callback': build_email_suc,
    'on_failure_callback': build_email_fail
}

spark_config = {
    'name': 'drp_app',
    'application': '/user/sav/drp/Scala_DRP-1.0.jar',
    'java_class': 'maksim0211.com.gitlab.Main',
    'jars' : '/user/sav/drp/hive-jdbc-1.1.0.jar',
    'jars' : '/user/sav/drp/ojdbc8-19.7.0.0.jar'
}

with DAG(
    	default_args=default_args,
    	dag_id="drp_dag",
    	schedule_interval="@daily",
    	catchup=False
) as dag:
    spark_operator = SparkSubmitOperator(
        task_id="drp_task",
        application_args=[
        	'HDFS_TABLEPROP_NAME=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/tableprop.txt',
		'URL=jdbc:oracle:thin:@192.168.233.20:1521:DWH',
                'ORACLE_OWNER_NAME=DWH',
		'HIVE_DB_NAME=test_msavelev',
		'PATH_KEYPAIR=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/resources/rsa.dat',
		'PATH_LOGPASS=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/resources/logpass.dat'
        ],
        **spark_config
    )

spark_operator